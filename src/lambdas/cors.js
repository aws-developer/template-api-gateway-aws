exports.handler = (event, context, _callback) => {
    const status_code_ok = 200;
    const originAllow = '*';
    const response = {
        statusCode: status_code_ok, 
        body: JSON.stringify({})
    };
    response.headers = {
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin": originAllow,
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
        "Cache-Control": "max-age=0, no-store, must-revalidate",
        "Content-Type": "application/json",
        "Content-Security-Policy": "frame-ancestors 'none'",
        "X-Frame-Options": "SAMEORIGIN"
    };
    context.success(response);
}